# identplatform/formula

[![Source Code]][source]
[![Version]][version]
[![PHP Version][php]

identplatform/formula is a PHP library for executing string formulas with data binds.


## Installation

The preferred method of installation is via [Composer][]. Run the following
command to install the package and add it as a requirement to your project's
`composer.json`:

Add to "repositories":
{
    "type": "vcs",
    "url":  "git@gitlab.com:identplatform/formula.git"
}

```bash
composer require identplatform/formula
```

## Documentation

### Example 1: BizRule
```php
use IdentPlatform\Formula\BoolFormula;

$boolFormula = new BoolFormula('$user->isOwner == TRUE && $product->id > 1000');

// bizrule somewhare in business logic
if ($boolFormula->execute([$user, $product])) {
    
}
```

### Example 2: 
```php
use IdentPlatform\Formula\FloatFormula;

$price = new FloatFormula('($good->price * $customer->tax * $bill->discount');
$cart = DB::Cart($cart_id);
$customer = DB::Customer($cart->customer_id);
$bill = DB::Bill($cart->bill_id);

// sum of goods in cart
$sum = 0.00;
foreach ($cart AS $good) {
    $price += $price->execute(['good' => $good, 'customer' => $customer, 'bill' => $bill);
}
```

## Contributing

Contributions are welcome!


## Copyright and License

The ramsey/uuid library is copyright © [IdentPlatform](https://identplatform.eu) and
licensed for use under the MIT License (MIT). Please see [LICENSE][] for more
information.

[source]: https://gitlab.com/identplatform/formula
[php]: https://php.net
[version]: 1.0