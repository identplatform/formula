<?php
namespace IdentPlatform\Formula;

use IdentPlatform\Formula\FormulaInterface;

/**
 * BoolFormula executor
 *
 * @author Petr Filla <petr.filla@gmail.com>
 * @version 1.0
 */
abstract class AbstractFormula implements FormulaInterface
{
    /**
     * @var string Formula
     */
    protected $formula = false;

    /**
     * @var bool Show error in execute
     */
    protected $showErrors = false;

    /**
     * Constructor
     * 
     * @param string $formula Formula
     * @param bool $showErrors Show errors
     * 
     * @since 1.0
     * @version 1.0
     */
    public function __construct(?string $formula, bool $showErrors = false)
    {
        $this->formula = $formula ?? null;
        $this->showErrors = $showErrors ?? false;
    }    

    abstract public function getType() : string;

    /**
     * {@inheritDoc}
     * 
     * @since 1.0
     * @version 1.0
     */
    public function execute(array $data = [])
    {
        $_result = null;
        $_code = "return ({$this->getType()}) ({$this->formula});";

        //check only vars in data
        $matches = null;
        preg_match_all('/\$[a-zA-Z][a-zA-Z0-9_]+/', $this->formula, $matches, PREG_OFFSET_CAPTURE);
        foreach ($matches[0] ?? [] as $key => $vars) {
            if (!array_key_exists(substr($vars[0], 1), $data)) throw new \InvalidArgumentException("Undefined var: {$vars[0]} in data key.");
        }

        // extract data for binding in eval()
        extract($data);

        // execute
        if ($this->formula !== '' && $this->formula !== null) {
            $_result = ($this->showErrors) ? eval($_code) : @eval($_code);
        } else {}

        return $_result;
    }
}