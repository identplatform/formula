<?php
namespace IdentPlatform\Formula;

use IdentPlatform\Formula\FormulaInterface;
use IdentPlatform\Formula\AbstractFormula;

/**
 * {@inheritDoc}
 */
class BoolFormula extends AbstractFormula implements FormulaInterface
{
    /**
     * {@inheritDoc}
     * 
     * @since 1.0
     * @version 1.0
     */
    public function getType() : string {
        return 'bool';
    }
}