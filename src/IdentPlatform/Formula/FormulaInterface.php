<?php
namespace IdentPlatform\Formula;

interface FormulaInterface
{
    /**
     * Executing formula by data
     *
     * @param array $data Associative array with data without "_" sign at first char
     * 
     * @throws \ParseError $formula parsing failed
     * @throws \InvalidArgumentException $data without keys in formula
     * @return mixed Result of formula
     */
    public function execute(array $data = []);

    /**
     * Type cast of formula
     * 
     * @return string Type cast result
     */
    public function getType() : string;
}

?>