<?php
namespace IdentPlatform\Formula\Tests\Unit;

use PHPUnit\Framework\TestCase;
use IdentPlatform\Formula\BoolFormula;

final class BoolFormulaTest extends TestCase
{
    public function testMethods()
    {
        $boolFormula = new BoolFormula(null);
        $reflection = new \ReflectionClass($boolFormula);
        $this->assertTrue($reflection->hasMethod('execute'));
    }

    /**
     * @dataProvider providerFormula
     */
    public function testExecuteFormula($formula, $data, $result)
    {
        $boolFormula = new BoolFormula((string) $formula);
        $this->assertEquals($boolFormula->execute((array) $data), $result);
    }

    /**
     * @dataProvider providerInvalidArgumentException
     */
    public function testExecuteInvalidArgumentException($formula, $data)
    {
        $this->expectException(\InvalidArgumentException::class);
        $boolFormula = new BoolFormula((string) $formula, true);
        $boolFormula->execute((array) $data);
    }

    /**
     * @dataProvider providerParseError
     */
    public function testExecuteParseError($formula, $data)
    {
        $this->expectException(\Error::class);
        $boolFormula = new BoolFormula((string) $formula);
        $this->assertFalse($boolFormula->execute((array) $data));
    }

    public function providerNull() {
        return [
            
        ];
    }

    public function providerFormula() {
        return [
            [null, null, null],
            ['', null, null],
            ['true', null, true],
            ['$hello == "Hello"', ['hello' => 'Hello'], true],
            ['$hello["name"] == "Hello"', ['hello' => ['name' => 'Hello']], true],
            ['5 == $counter', ['counter' => 5], true],
            ['false', null, false],
            ['$hello == "Hello"', ['hello' => 'HELLO'], false],
            ['$hello["name"] == "Hello"', ['hello' => ['name' => 'HELLO']], false],
            ['5 > $counter', ['counter' => 5], false],
        ];
    }

    public function providerInvalidArgumentException() {
        return [
            ['$hello == "Hello"', null],
            ['$hello == "Hello"', []],
            ['$hello == "Hello"', ['Hello' => 'Hello']],
        ];
    }

    public function providerParseError() {
        return [
            ['emtpy()', []],
        ];
    }
}
