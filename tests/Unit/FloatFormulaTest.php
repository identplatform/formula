<?php
namespace IdentPlatform\Formula\Tests\Unit;

use PHPUnit\Framework\TestCase;
use IdentPlatform\Formula\FloatFormula;

final class FloatFormulaTest extends TestCase
{
    public function testMethods()
    {
        $boolFormula = new FloatFormula(null);
        $reflection = new \ReflectionClass($boolFormula);
        $this->assertTrue($reflection->hasMethod('execute'));
    }

    /**
     * @dataProvider providerFormula
     */
    public function testExecuteFomulas($formula, $data, $result)
    {
        $boolFormula = new FloatFormula((string) $formula);
        $this->assertEquals($boolFormula->execute((array) $data), $result);
    }

    /**
     * @dataProvider providerInvalidArgumentException
     */
    public function testExecuteInvalidArgumentException($formula, $data)
    {
        $this->expectException(\InvalidArgumentException::class);
        $boolFormula = new FloatFormula((string) $formula, true);
        $boolFormula->execute((array) $data);
    }

    /**
     * @dataProvider providerParseError
     */
    public function testExecuteParseError($formula, $data)
    {
        $this->expectException(\Error::class);
        $boolFormula = new FloatFormula((string) $formula);
        $this->assertFalse($boolFormula->execute((array) $data));
    }

    public function providerFormula() {
        return [
            [null, null, null],
            ['', null, null],
            ['5', null, 5],
            ['$number * 2', ['number' => 5.5], 11],
            ['$number * 2', ['number' => '5.5'], 11],
            ['$data["count"]', ['data' => ['count' => 1]], 1],
            ['$num1 + $num2', ['num1' => 1, 'num2' => 2], 3],
        ];
    }

    public function providerInvalidArgumentException() {
        return [
            ['$count + 5.5', []],
            ['$count + 5.5"', ['number' => 5.5]],
        ];
    }

    public function providerParseError() {
        return [
            ['5.5 +', null],
            ['emtpy()', []],
        ];
    }
}
